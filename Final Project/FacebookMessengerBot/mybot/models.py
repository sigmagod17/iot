from django.db import models

# Create your models here.

# coding: utf-8

# In[1]:
from IPython import get_ipython
import pymysql
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import numpy as np
import jieba, os, io
import jieba.posseg as pseg
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LinearRegression
# from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import random
import string
import dropbox

jieba.load_userdict('./NameDict_Ch_v2.txt')
DrpoBoxToken = 'aLkeqSiZKVgAAAAAAAAWfeYJOSAZJ0LwHqQ7kTGn3prezfPv194WSSxxSW7Jk3R6'
dbx = dropbox.Dropbox(DrpoBoxToken)


# In[36]:

def getData(area, bulid):
    db = pymysql.connect(user='user', password='user', host='140.120.13.245', database='TC_House', port=43306,
                         charset='utf8')
    cursor = db.cursor()
    # cursor.execute("SELECT * FROM TC_House WHERE towns = '北屯區'")
    train = ''
    recordcount = 0
    if area != '' or bulid != '':
        # print("select * from TC_House where towns like %s and building_state like %s and total_price < 30000000", ('%中區%', '%大樓%'))
        # print(sqlcom)
        cursor.execute(
            "select * from TC_House where towns like %s and building_state like %s and total_price < 30000000",
            ('%' + area + '%', '%' + bulid + '%'))
        result = cursor.fetchall()
        recordcount = cursor.rowcount
        columnName = ['seq_no', 'towns', 'transaction_sign', 'building_plate', 'total_area', 'transaction_time',
                      'transaction_time_year', 'transaction_time_month', 'shifting_level', 'total_floor',
                      'total_floor_type', 'building_state', 'complete_time', 'age', 'age_type', 'building_total_area',
                      'building_room', 'building_hall', 'building_health', 'is_building_compartmented', 'is_manages',
                      'total_price', 'unit_price']
        # print(len(columnName))
        train = pd.DataFrame(list(result), columns=columnName)
        print(recordcount)
        db.close()
    else:
        db.close()

    # test = train.sample(1000)      #隨機從train中取得1100筆做為測試資料
    # idx = list(test.index.values) #取得test中的index
    # train2 = train.drop(train.index[idx]) #從測試資料中刪除test資料
    # print(train2['total_price'])
    return train, recordcount


# In[3]:

def getItemName(train,test): #這個method用來檢驗train跟test中有無不一樣的item
    testRe = []
    trainRe = []
    for record in test:
        testRe.append(record)
    for record in train:
        trainRe.append(record)
    #print('test =',len(testRe),'train =',len(trainRe))
    s1 = set(testRe)
    s2 = set(trainRe)
    #print('Test沒有的項目：',list(s1.symmetric_difference(s2))) #test中沒有的Item
    result = list(s1.intersection(s2))
    result = ['building_total_area','total_floor_type']#這邊挑選需要的欄位
#     result.remove('transaction_time')  #remove掉一些文字資料及datetime類別的資料，把它變成數值資料後太複雜，所以先刪掉
#     result.remove('complete_time')
#     result.remove('transaction_sign')
#     result.remove('building_plate')
#     result.remove('seq_no')
#     result.remove('building_state')
#     result.remove('shifting_level')
    #print(result)
    return result,len(result)          #test和train中重複的Item


# In[4]:

def datafilter_month_v2(data,start,end):
    # monthIndex = data.set_index('transaction_time_month')
    filter1 = data['transaction_time_month'] > start
    ms = data[filter1]
    filter2 = ms['transaction_time_month'] < end
    ms2 = ms[filter2]
    #middle = monthIndex[(monthIndex.index > start) & (monthIndex.index < end)]
    #print(ms2.info())
    return ms2


# In[5]:

def prepareXY(trainP, testP):
    # print(test['total_price'])
    ItemList, l = getItemName(trainP, testP)  # l 這個變數是為了如果要使用keras時留下來的，作用在於紀錄總共使用幾個特徵來學習
    X_train = trainP[ItemList]
    y_train = trainP['total_price']

    X_test = testP[ItemList]
    y_test = testP['total_price']  # 這邊的y_test就是測試資料的值
    return X_train, y_train, X_test, y_test


# In[1]:

def trainAndPredict(train):
    scmean = 0.0
    predictList = []
    realList = []
    yearMonList = []
    # scoreList = []
    for year in range(2012, 2018):
        for month in range(1, 13):
            #             if (year == 2012):
            #                 if (month >= 5):
            #                     trainYearInUse = train[train['transaction_time_year'] == year]
            #                     seasonTrain = datafilter_month_v2(trainYearInUse,(month-5),month)

            #                     #seasonTest = datafilter_month_v2(trainYearInUse,(month-1),(month+1))
            #                     train8, test2 = train_test_split(seasonTrain, test_size = 0.2)
            #                     X_train,y_train,X_test,y_test = prepareXY(train8, test2)
            #                     df = pd.DataFrame(y_train)
            #                     y_trains = df['total_price'].astype('int64')
            #                     df2 = pd.DataFrame(y_test)
            #                     y_tests = df2['total_price'].astype('int64')
            #                     #print(year,month,len(X_train))
            #                     yearMonList.append(str(year) + '-' + str(month).zfill(2))
            #                     try:
            #                         t,sc = RandomFCF(X_train,y_trains,X_test,y_tests)# 這邊的t就是預測出來的值
            #                     except:
            #                         return trainAndPredict_byYear(train)
            #                     realList.append(y_tests)
            #                     predictList.append(t)
            #                     scmean  = sc
            #             else:
            if (month < 5):
                if (month == 1):
                    trainYearInUse = train[train['transaction_time_year'] == (year - 1)]
                    seasonTrain = datafilter_month_v2(trainYearInUse, 8, 13)
                    trainYearInUse = train[train['transaction_time_year'] == year]
                    # seasonTest = datafilter_month_v2(trainYearInUse,(month-1),(month+1))

                    train8, test2 = train_test_split(seasonTrain, test_size=0.2)
                    X_train, y_train, X_test, y_test = prepareXY(train8, test2)
                    df = pd.DataFrame(y_train)
                    y_trains = df['total_price'].astype('int64')
                    df2 = pd.DataFrame(y_test)
                    y_tests = df2['total_price'].astype('int64')
                    # print(year,month,len(X_train))
                    yearMonList.append(str(year) + '-' + str(month).zfill(2))
                    try:
                        t, sc = RandomFCF(X_train, y_trains, X_test, y_tests)  # 這邊的t就是預測出來的值
                    except:
                        return trainAndPredict_byYear(train)
                    realList.append(y_tests)
                    predictList.append(t)
                    scmean += sc
                elif (month == 2):
                    trainYearInUse = train[train['transaction_time_year'] == (year - 1)]
                    t1 = datafilter_month_v2(trainYearInUse, 9, 13)
                    trainYearInUse = train[train['transaction_time_year'] == year]
                    t2 = datafilter_month_v2(trainYearInUse, 0, 2)
                    f = [t1, t2]
                    seasonTrain = pd.concat(f)
                    # seasonTest = datafilter_month_v2(trainYearInUse,(month-1),(month+1))

                    train8, test2 = train_test_split(seasonTrain, test_size=0.2)
                    X_train, y_train, X_test, y_test = prepareXY(train8, test2)
                    df = pd.DataFrame(y_train)
                    y_trains = df['total_price'].astype('int64')
                    df2 = pd.DataFrame(y_test)
                    y_tests = df2['total_price'].astype('int64')
                    # print(year,month,len(X_train))
                    yearMonList.append(str(year) + '-' + str(month).zfill(2))
                    try:
                        t, sc = RandomFCF(X_train, y_trains, X_test, y_tests)  # 這邊的t就是預測出來的值
                    except:
                        return trainAndPredict_byYear(train)
                    realList.append(y_tests)
                    predictList.append(t)
                    scmean += sc
                elif (month == 3):
                    trainYearInUse = train[train['transaction_time_year'] == (year - 1)]
                    t1 = datafilter_month_v2(trainYearInUse, 10, 13)
                    trainYearInUse = train[train['transaction_time_year'] == year]
                    t2 = datafilter_month_v2(trainYearInUse, 0, 3)
                    f = [t1, t2]
                    seasonTrain = pd.concat(f)
                    # seasonTest = datafilter_month_v2(trainYearInUse,(month-1),(month+1))

                    train8, test2 = train_test_split(seasonTrain, test_size=0.2)
                    X_train, y_train, X_test, y_test = prepareXY(train8, test2)
                    df = pd.DataFrame(y_train)
                    y_trains = df['total_price'].astype('int64')
                    df2 = pd.DataFrame(y_test)
                    y_tests = df2['total_price'].astype('int64')
                    # print(year,month,len(X_train))
                    yearMonList.append(str(year) + '-' + str(month).zfill(2))
                    try:
                        t, sc = RandomFCF(X_train, y_trains, X_test, y_tests)  # 這邊的t就是預測出來的值
                    except:
                        return trainAndPredict_byYear(train)
                    realList.append(y_tests)
                    predictList.append(t)
                    scmean += sc
                else:
                    trainYearInUse = train[train['transaction_time_year'] == (year - 1)]
                    t1 = datafilter_month_v2(trainYearInUse, 10, 13)
                    trainYearInUse = train[train['transaction_time_year'] == year]
                    t2 = datafilter_month_v2(trainYearInUse, 0, 3)
                    f = [t1, t2]
                    seasonTrain = pd.concat(f)
                    # seasonTest = datafilter_month_v2(trainYearInUse,(month-1),(month+1))

                    train8, test2 = train_test_split(seasonTrain, test_size=0.2)
                    X_train, y_train, X_test, y_test = prepareXY(train8, test2)
                    df = pd.DataFrame(y_train)
                    y_trains = df['total_price'].astype('int64')
                    df2 = pd.DataFrame(y_test)
                    y_tests = df2['total_price'].astype('int64')
                    # print(year,month,len(X_train))
                    yearMonList.append(str(year) + '-' + str(month).zfill(2))
                    try:
                        t, sc = RandomFCF(X_train, y_trains, X_test, y_tests)  # 這邊的t就是預測出來的值
                    except:
                        return trainAndPredict_byYear(train)
                    realList.append(y_tests)
                    predictList.append(t)
                    scmean += sc
            else:
                trainYearInUse = train[train['transaction_time_year'] == year]
                seasonTrain = datafilter_month_v2(trainYearInUse, (month - 5), month)
                # seasonTest = datafilter_month_v2(trainYearInUse,(month-1),(month+1))
                # print(year,month,len(seasonTrain))
                yearMonList.append(str(year) + '-' + str(month).zfill(2))
                train8, test2 = train_test_split(seasonTrain, test_size=0.2)
                X_train, y_train, X_test, y_test = prepareXY(train8, test2)
                df = pd.DataFrame(y_train)
                y_trains = df['total_price'].astype('int64')
                df2 = pd.DataFrame(y_test)
                y_tests = df2['total_price'].astype('int64')
                try:
                    t, sc = RandomFCF(X_train, y_trains, X_test, y_tests)  # 這邊的t就是預測出來的值
                except:
                    return trainAndPredict_byYear(train)
                realList.append(y_tests)
                predictList.append(t)
                scmean += sc
    # print(len(predictList), len(realList))
    scmean = scmean / len(predictList)
    return predictList, realList, yearMonList, scmean


# In[7]:

def RandomFCF(x_trainR, y_trainR, X_testR, y_testR):
    # print(len(y_testR))
    model = LinearRegression()
    model.fit(x_trainR, y_trainR)
    sc = model.score(X_testR, y_testR)
    return model.predict(X_testR), sc


# In[20]:

def sqlOrderCreate(jiebaList):
    jiebaList = jieba.lcut(jiebaList)
    areaDict = {'南屯區': '南屯區', '南屯': '南屯', '北屯區': '北屯區', '北屯': '北屯', '北區': '北區',
                '中區': '中區', '西區': '西區', '南區': '南區', '東區': '東區', '西屯區': '西屯區',
                '西屯': '西屯', '大雅區': '大雅區', '大雅': '大雅', '大里區': '大里區', '大里': '大里',
                '太平區': '太平區', '太平': '太平', '沙鹿區': '沙鹿區', '沙鹿': '沙鹿', '龍井': '龍井',
                '龍井區': '龍井區', '烏日區': '烏日區', '烏日': '烏日', '潭子區': '潭子區', '潭子': '潭子',
                '霧峰區': '霧峰區', '霧峰': '霧峰', '豐原區': '豐原區', '豐原': '豐原', '后里區': '后里區',
                '后里': '后里', '石岡區': '石岡區', '石岡': '石岡', '東勢區': '東勢區', '東勢': '東勢',
                '和平區': ' 和平區', '和平': '和平', '新社': '新社', '神岡區': '神岡區', '神岡': '神岡',
                '大肚區': '大肚區', '大肚': '大肚', '梧棲區': '梧棲區', '梧棲': '梧棲', '清水區': '清水區',
                '清水': '清水', '大甲區': '大甲區', '大甲': '大甲', '大安區': '大安區', '大安': '大安',
                '外埔區': '外埔區', '外埔': '外埔'}
    buildingDict = {'華廈': '華廈(10層含以下有電梯)', '大樓': '住宅大樓(11層含以上有電梯)', '公寓': '公寓(5樓含以下無電梯)', '透天厝': '透天厝', '透天': '透天厝',
                    '套房': '套房(1房1廳1衛)', '辦公商業大樓': '辦公商業大樓', '店面': '店面(店鋪)', '工廠': '工廠', '廠辦': '廠辦', '農舍': '農舍'}
    roomDict = {'四房': 4, '4房': 4, '三房': 3, '3房': 3, '兩房': 2, '2房': 2}  # 房
    hallDict = {'兩廳': 2, '2廳': 2}  # 廳
    healthDict = {'兩衛': 2, '2衛': 2, '一衛': 1, '1衛': 1}  # 衛
    # commandList = ["SELECT * FROM TC_House WHERE " ,"towns = "]
    # sqlcommand = ""
    # building = ""
    #     for i in jiebaList:        # 如果說沒有指定區域的話就直接預測整個台中的房價給他
    #         if (i in areaDict.keys()):
    #             print(i)
    #             break
    #         else:
    #             sqlcommand = "SELECT * FROM TC_House"
    #             return sqlcommand
    area = ''
    build = ''
    for i in jiebaList:
        # print(i)
        if (i in areaDict.keys()):
            area = areaDict.get(i)  # 地區
        # else:
        #     area = ''
        if (i in buildingDict.keys()):
            build = buildingDict.get(i)
        # else:
        #     build = ''
        if (i in roomDict.keys()):
            room = roomDict.get(i)
        if (i in hallDict.keys()):
            hall = hallDict.get(i)
        if (i in healthDict.keys()):
            health = healthDict.get(i)
    # commandList.append(" and total_price < 30000000 ")
    # sqlcommand = sqlcommand.join(commandList)

    # sqlcommand = "select * from TC_House where towns like %s and building_state like %s and total_price < 30000000" + ", (%" + area + "%, %" + building + "%)"
    # print('area:' + area + 'build:' + build)
    # return sqlcommand,area,building
    return area, build


# In[88]:

def drawdiagram(predictList, realList, yearMonList, area, building, pmean):
    myfont = FontProperties(fname=r'./msb.ttf')
    # get_ipython().magic(u'matplotlib')
    indexforMat = []
    indexforMat2 = []
    listTest = []
    for nda in predictList:
        indexforMat.append(nda.mean())
    for nda in realList:
        indexforMat2.append(nda.mean())

    x = range(len(indexforMat))

    s = area + building + "的房價預測"
    # ，Model Score：" + "{0:.5f}".format(pmean)
    plt.figure(figsize=(12, 8))
    realLine, = plt.plot(x, indexforMat2, '-g', linewidth=1, label='up')
    predictLine, = plt.plot(x, indexforMat, '-r', linewidth=1, label='down')

    plt.title(s, FontProperties=myfont, fontsize=16)
    plt.xticks(x, yearMonList, rotation='45')
    plt.xlabel('年（月）', FontProperties=myfont, fontsize=14)
    plt.ylabel('價格', FontProperties=myfont, fontsize=14)

    plt.legend(handles=[realLine, predictLine, ], labels=['Real', 'Predict'], loc='best')

    fileName = ''.join(random.choices(string.ascii_uppercase + string.digits + string.ascii_lowercase, k=10))
    buf = io.BytesIO()
    # print(type(buf))
    plt.savefig(buf, format="jpg")
    buf.seek(0)

    # with open(buf, "rb") as f:
    dbx.files_upload(buf.read(), '/' + fileName + '.jpg', mute=True)

    # path = '/beach.jpg'
    link = dbx.sharing_create_shared_link_with_settings('/' + fileName + '.jpg').url
    return link


def trainAndPredict_byYear(train):
    scmean = 0.0
    predictList = []
    realList = []
    yearMonList = []
    # scoreList = []
    for year in range(2012, 2018):
        trainYearInUse = train[train['transaction_time_year'] == (year)]
        # seasonTrain = datafilter_month_v2(trainYearInUse,(month-5),month)
        # seasonTest = datafilter_month_v2(trainYearInUse,(month-1),(month+1))
        # print(year,month,len(seasonTrain))
        yearMonList.append(str(year))
        train8, test2 = train_test_split(trainYearInUse, test_size=0.2)
        X_train, y_train, X_test, y_test = prepareXY(train8, test2)
        df = pd.DataFrame(y_train)
        y_trains = df['total_price'].astype('int64')
        df2 = pd.DataFrame(y_test)
        y_tests = df2['total_price'].astype('int64')
        t, sc = RandomFCF(X_train, y_trains, X_test, y_tests)  # 這邊的t就是預測出來的值
        realList.append(y_tests)
        predictList.append(t)
        scmean += sc

    # print(len(predictList), len(realList))
    scmean = scmean / len(predictList)
    return predictList, realList, yearMonList, scmean

# train2 = getData('1234')